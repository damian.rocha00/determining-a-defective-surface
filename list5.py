import numpy as np
import matplotlib.pyplot as plt

def transformacja_1(point):
    x, y = point
    x_new = 0.85 * x + 0.04 * y - 1
    y_new = -0.04 * x + 0.85 * y + 2.6
    return x_new, y_new
def transformacja_1_w_lewo(point):
    x, y = point
    x_new = 0.85 * x - 0.04 * y
    y_new = 0.04 * x + 0.85 * y + 1.6
    return x_new, y_new

def transformacja_2(point):
    x, y = point
    x_new = 0.20 * x - 0.26 * y
    y_new = 0.23 * x + 0.22 * y + 1.6
    return x_new, y_new
def transformacja_3(point):
    x, y = point
    x_new = -0.15 * x + 0.28 * y
    y_new = 0.26 * x + 0.24 * y + 0.44
    return x_new, y_new
def transformacja_4(point):
    x, y = point
    x_new = 0.0
    y_new = 0.16 * y
    return x_new, y_new
p = [0.85, 0.92, 0.99, 1.0]

def generuj_kwiat_paproci(num_points, r):
    points = [(0.5, 0.5)]
    x = np.array([0.5, 0.5])
    for j in range(num_points):
        if r == 1:
            x = transformacja_1(x)
        elif r == 2:
            x = transformacja_2(x)
        elif r == 3:
            x = transformacja_3(x)
        else:
            x = transformacja_4(x)
        points.append(tuple(x))
    return points

def generuj_kwiat_paproci_w_lewo(num_points):
    points = [(0.5, 0.5)]
    x = np.array([0.5, 0.5])
    for j in range(num_points):
        r = np.random.rand()
        if r < p[0]:
            x = transformacja_1_w_lewo(x)
        elif r < p[1]:
            x = transformacja_2(x)
        elif r < p[2]:
            x = transformacja_3(x)
        else:
            x = transformacja_4(x)
        points.append(tuple(x))
    return points

num_points = 100000
points1 = generuj_kwiat_paproci(num_points, 1)
points2 = generuj_kwiat_paproci(num_points, 2)
points3 = generuj_kwiat_paproci(num_points, 3)
points4 = generuj_kwiat_paproci(num_points, 4)
x_p1, y_p1 = zip(*points1)
x_p2, y_p2 = zip(*points2)
x_p3, y_p3 = zip(*points3)
x_p4, y_p4 = zip(*points4)

pointsL = generuj_kwiat_paproci_w_lewo(num_points)
x_pL, y_pL = zip(*pointsL)

fig, ax = plt.subplots(1, 2, figsize=(10, 5), sharex=True, sharey=True)

ax[0].plot(x_p1, y_p1, 'v-r')
ax[0].plot(x_p2, y_p2, 'o-b')
ax[0].plot(x_p3, y_p3, 's-g')
ax[0].plot(x_p4, y_p4, 'D-k')
ax[0].set_title("Kwiat paproci w prawo")

ax[1].scatter(x_pL, y_pL, s=0.4, c='green')
ax[1].set_title("Kwiat paproci w lewo")

# plt.axis([-3, 3, 0, 10])
plt.show()