
import math
import numpy as np
import matplotlib.pyplot as plt
import skimage
from skimage import io
from skimage import data
from skimage import color
from skimage import filters



# plot histogramu poszczególnych warstw obrazu
def plot_hist_warstwa():
    obraz = io.imread('natura.jpg')
    def plot_hist_warstwa(obraz, warstwa):
        w = ["red", "green", "blue"]
        warstwa_index = w.index(warstwa)
        color = w[warstwa_index]

        wybrana_warstwa = obraz[:, :, warstwa_index]
        fig, (ax1, ax2) = plt.subplots(
            ncols=2, figsize=(18, 6)
        )

        ax1.imshow(obraz)
        ax1.axis("off")
        ax2.hist(wybrana_warstwa.ravel(), bins=256, color=color)
        ax2.set_title(f"{w[warstwa_index]} histogram")
        ax2.grid()

    plot_hist_warstwa(obraz, "green")
    plt.show()

def plot_w_skali_szarosci():
    # plot histogramu obrazu w skali szarości
    obraz = io.imread('natura.jpg')
    gray_color_obraz = color.rgb2gray(obraz)

    fig, ax = plt.subplots(ncols=2)
    ax[0].imshow(gray_color_obraz, cmap=plt.cm.gray)
    ax[1].hist(gray_color_obraz.ravel(), bins=256)
    ax[1].grid()
############################################
    # progowanie ręczne (manual thresholding)
    print(gray_color_obraz.mean())

    # ustawienie progu
    threshold = 0.55

    # binearyzacja - obraz monochromatyczny
    binary_obraz = gray_color_obraz > threshold
    # odwrotna binearyzacja
    inverted_binary_obraz = gray_color_obraz <= threshold

    fig, ax = plt.subplots(ncols=3)
    ax[0].imshow(gray_color_obraz, cmap=plt.cm.gray)
    ax[1].imshow(binary_obraz, cmap=plt.cm.gray)
    ax[2].imshow(inverted_binary_obraz, cmap=plt.cm.gray)
    plt.show()

def coins():
    gray_obraz = data.coins()

    print(gray_obraz.dtype)
    print(gray_obraz.mean())

    fig, ax = plt.subplots(ncols=2)
    ax[0].imshow(gray_obraz, cmap=plt.cm.gray)
    ax[1].hist(gray_obraz.ravel(), bins=256)
    ax[1].grid()
###########################
    # parametry obrazu
    gray_obraz.dtype
    gray_obraz.shape
    gray_obraz.ndim
    gray_obraz.size

    # ustawienie progu
    threshold1 = 100
    threshold2 = 150

    # binearyzacja
    binary_obraz1 = gray_obraz > threshold1
    binary_obraz2 = gray_obraz > threshold2

    fig, ax = plt.subplots(ncols=3)
    ax[0].imshow(gray_obraz, cmap=plt.cm.gray)
    ax[1].imshow(binary_obraz1, cmap=plt.cm.gray)
    ax[2].imshow(binary_obraz2, cmap=plt.cm.gray)
    plt.show()

##############################################
    # progowanie metoda Otsu (Otsu threshold)

    threshold = filters.threshold_otsu(gray_obraz)
    binary = gray_obraz > threshold

    fig, ax = plt.subplots(ncols=3, figsize=(8, 2.5))

    ax[0].imshow(gray_obraz, cmap=plt.cm.gray)
    ax[0].set_title('Obraz oryginalny')
    ax[0].axis('off')

    ax[1].hist(gray_obraz.ravel(), bins=256)
    ax[1].set_title('Histogram')
    ax[1].axvline(threshold, color='r')
    ax[1].grid()

    ax[2].imshow(binary, cmap=plt.cm.gray)
    ax[2].set_title('Obraz binarny')
    ax[2].axis('off')

    plt.show()


def camera():
    # progowanie globalne (global threshold)

    gray_obraz = data.camera()
    # gray_obraz = data.page()

    fig, ax = filters.try_all_threshold(gray_obraz, figsize=(10, 8), verbose=False)
    plt.show()

def progowanie_lokalne():
    # progowanie lokalne (local threshold)

    obraz = io.imread(
        'wzory_math.jpg')
    gray_color_obraz = color.rgb2gray(obraz)

    local_threshold = filters.threshold_local(gray_color_obraz, block_size=3, method='gaussian', offset=0.001)

    binary = gray_color_obraz > local_threshold

    fig, ax = plt.subplots(ncols=3)
    ax[0].imshow(obraz, cmap=plt.cm.gray)
    ax[1].imshow(gray_color_obraz, cmap=plt.cm.gray)
    ax[2].imshow(binary, cmap=plt.cm.gray)
    plt.show()


def porownanie_progowan():

    # porównanie progowania globalnego i lokalnego (1)

    obraz = io.imread('sudoku.jpg')
    gray_color_obraz = color.rgb2gray(obraz)

    local_threshold = filters.threshold_local(gray_color_obraz, block_size=3, method='gaussian', offset=0.0003)
    binary_local = gray_color_obraz > local_threshold

    threshold = filters.threshold_otsu(gray_color_obraz)
    binary_otsu = gray_color_obraz > threshold

    fig, ax = plt.subplots(ncols=3)
    ax[0].imshow(gray_color_obraz, cmap=plt.cm.gray)
    ax[1].imshow(binary_otsu, cmap=plt.cm.gray)
    ax[2].imshow(binary_local, cmap=plt.cm.gray)
    plt.show()

def porownanie_progowan2():
    # porównanie progowania globalnego i lokalnego (2)

    gray_color_obraz = data.page()

    local_threshold = filters.threshold_local(gray_color_obraz, block_size=35, offset=10)
    binary_local = gray_color_obraz > local_threshold

    threshold = filters.threshold_otsu(gray_color_obraz)
    binary_otsu = gray_color_obraz > threshold

    fig, ax = plt.subplots(ncols=3)
    ax[0].imshow(gray_color_obraz, cmap=plt.cm.gray)
    ax[1].imshow(binary_otsu, cmap=plt.cm.gray)
    ax[2].imshow(binary_local, cmap=plt.cm.gray)
    plt.show()

if __name__ == "__main__":
    plot_w_skali_szarosci()

