import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import skimage as ski
from skimage import morphology
from skimage import io
from skimage import data
from skimage import color
from skimage import filters
# obrazy binarne
img1 = io.imread('morphology1.png')
img1 = color.rgb2gray(img1)
threshold = filters.threshold_otsu(img1)
binary1 = (img1 > threshold)*1

img2 = io.imread('morphology4.png')
threshold = filters.threshold_otsu(img2)
binary2 = (img2 > threshold)*1

# operacja rozszerzenia

#img_d = morphology.binary_dilation(binary1)
img_d = sp.ndimage.binary_dilation(binary2, iterations = 2)*1

fig, ax = plt.subplots(ncols=2)
ax[0].imshow(binary2, cmap=plt.cm.gray)
ax[1].imshow(img_d, cmap=plt.cm.gray)