import matplotlib.pyplot as plt
from skimage import io
import numpy as np

def printDworek():
    dworek = io.imread('dworek.jpg')
    plt.imshow(dworek)
    plt.show()

def printRatusz():
    ratuszZG = io.imread('ratuszZG.jpg')
    plt.imshow(ratuszZG)
    plt.show()
    # print(obraz.dtype)
    # print(obraz.min())
    # plt.imshow(ratuszZG[:,:,0])

def printRaturHour():
    ratuszZG = io.imread('ratuszZG.jpg')
    elementRatuszZG = ratuszZG[180:220, 380:420, :]
    (m, n, p) = elementRatuszZG.shape
    red = np.zeros((m, n, p), dtype='uint8')
    green = np.zeros((m, n, p), dtype='uint8')
    blue = np.zeros((m, n, p), dtype='uint8')

    red[:, :, 0] = elementRatuszZG[:, :, 0]
    green[:, :, 1] = elementRatuszZG[:, :, 1]
    blue[:, :, 2] = elementRatuszZG[:, :, 2]

    fig, ax = plt.subplots(2, 2, figsize=(10, 5), sharex=True, sharey=True)
    im1 = ax[0, 0].imshow(elementRatuszZG)
    im2 = ax[0, 1].imshow(red)
    im3 = ax[1, 0].imshow(green)
    im4 = ax[1, 1].imshow(blue)
    plt.show()

# def printGrey():


if __name__ == "__main__":
    # printDworek()
    # printRatusz()
    printRaturHour()