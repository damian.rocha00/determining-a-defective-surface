import matplotlib.pyplot as plt
from skimage import io, data, img_as_float, exposure, color
import numpy as np
import matplotlib

def list3Task1():
    papierowyDom = io.imread('papierowyDom.jpg')
    papierowyDomZnormalizowany = papierowyDom/255

    transformacja1_64 = papierowyDomZnormalizowany/64
    transformacja2_64 = 64 * transformacja1_64

    transformacja1_32 = papierowyDomZnormalizowany/32
    transformacja2_32 = 32 * transformacja1_32

    transformacja1_16 = papierowyDomZnormalizowany/2
    transformacja2_16 = 2 * transformacja1_16

    fig, ax = plt.subplots(3, 3, figsize=(10, 5), sharex=True, sharey=True)
    im1 = ax[0, 0].imshow(papierowyDomZnormalizowany)
    im2 = ax[0, 1].imshow(transformacja1_64)
    im3 = ax[0, 2].imshow(transformacja2_64)

    im4 = ax[1, 0].imshow(papierowyDomZnormalizowany)
    im5 = ax[1, 1].imshow(transformacja1_32)
    im6 = ax[1, 2].imshow(transformacja2_32)

    im7 = ax[2, 0].imshow(papierowyDomZnormalizowany)
    im8 = ax[2, 1].imshow(transformacja1_16)
    im9 = ax[2, 2].imshow(transformacja2_16)
    plt.show()

def list3Task2():
    dworek = io.imread('Dworek.jpg')
    print("typ: ", dworek.dtype, "min: ", dworek.min(), "max: ", dworek.max())

    dworekZnormalizowany = dworek/255

    transformacja1 = dworekZnormalizowany ** 2

    transformacja2 = dworekZnormalizowany ** (1/2)

    transformacja3 = 2 * dworekZnormalizowany ** (8/10)

    fig, ax = plt.subplots(2, 2, figsize=(10, 5), sharex=True, sharey=True)
    im1 = ax[0, 0].imshow(dworekZnormalizowany)
    im2 = ax[0, 1].imshow(transformacja1)
    im3 = ax[1, 0].imshow(transformacja2)
    im4 = ax[1, 1].imshow(transformacja3)

    plt.show()

def list3Task3(s1, t1, s2, t2):
    dworek = io.imread('Dworek.jpg')
    print("typ: ", dworek.dtype, "min: ", dworek.min(), "max: ", dworek.max())

    dworekZnormalizowany = dworek/255

    t1/s1*dworek
    (t2 - t1) / (s2 - s1) * (dworek - s1) + t1
    255 - t2 * (x - s2 ) + t2

    transformacja1 = dworekZnormalizowany ** 2

    transformacja2 = dworekZnormalizowany ** (1/2)

    transformacja3 = 2 * dworekZnormalizowany ** (8/10)

    fig, ax = plt.subplots(2, 2, figsize=(10, 5), sharex=True, sharey=True)
    im1 = ax[0, 0].imshow(dworekZnormalizowany)
    im2 = ax[0, 1].imshow(transformacja1)
    im3 = ax[1, 0].imshow(transformacja2)
    im4 = ax[1, 1].imshow(transformacja3)

    plt.show()



matplotlib.rcParams['font.size'] = 8


import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from skimage import data, img_as_float
from skimage import exposure


matplotlib.rcParams['font.size'] = 8


def plot_img_and_hist(image, axes, bins=256):
    """Plot an image along with its histogram and cumulative histogram.

    """
    image = img_as_float(image)
    ax_img, ax_hist = axes
    ax_cdf = ax_hist.twinx()

    # Display image
    ax_img.imshow(image, cmap=plt.cm.gray)
    ax_img.set_axis_off()

    # Display histogram
    ax_hist.hist(image.ravel(), bins=bins, histtype='step', color='black')
    ax_hist.ticklabel_format(axis='y', style='scientific', scilimits=(0, 0))
    ax_hist.set_xlabel('Pixel intensity')
    ax_hist.set_xlim(0, 1)
    ax_hist.set_yticks([])

    # Display cumulative distribution
    img_cdf, bins = exposure.cumulative_distribution(image, bins)
    ax_cdf.plot(bins, img_cdf, 'r')
    ax_cdf.set_yticks([])

    return ax_img, ax_hist, ax_cdf


# Load an example image
img = io.imread('Dworek.jpg')
img = img/255
R = img[:, :, 0]
G = img[:, :, 1]
B = img[:, :, 2]
imgBW = (R*0.299 + G*0.587 + B*0.114)
newImg = np.zeros_like(img)
eps = np.finfo(float).eps

# Contrast stretching
p2, p98 = np.percentile(img, (2, 98))
img_rescale = exposure.rescale_intensity(img, in_range=(p2, p98))

# Equalization


img_eq = exposure.equalize_hist(img)
img[:, :, 0] = (img_eq/(imgBW+eps))*R
img[:, :, 1] = (img_eq/(imgBW+eps))*G
img[:, :, 2] = (img_eq/(imgBW+eps))*B

# Adaptive Equalization
img_adapteq = exposure.equalize_adapthist(img, clip_limit=0.03)

# Display results
fig = plt.figure(figsize=(8, 5))
axes = np.zeros((2, 4), dtype=object)
axes[0, 0] = fig.add_subplot(2, 4, 1)
for i in range(1, 4):
    axes[0, i] = fig.add_subplot(2, 4, 1+i, sharex=axes[0,0], sharey=axes[0,0])
for i in range(0, 4):
    axes[1, i] = fig.add_subplot(2, 4, 5+i)

ax_img, ax_hist, ax_cdf = plot_img_and_hist(img, axes[:, 0])
ax_img.set_title('Low contrast image')

y_min, y_max = ax_hist.get_ylim()
ax_hist.set_ylabel('Number of pixels')
ax_hist.set_yticks(np.linspace(0, y_max, 5))

ax_img, ax_hist, ax_cdf = plot_img_and_hist(img_rescale, axes[:, 1])
ax_img.set_title('Contrast stretching')

ax_img, ax_hist, ax_cdf = plot_img_and_hist(img_eq, axes[:, 2])
ax_img.set_title('Histogram equalization')

ax_img, ax_hist, ax_cdf = plot_img_and_hist(img_adapteq, axes[:, 3])
ax_img.set_title('Adaptive equalization')

ax_cdf.set_ylabel('Fraction of total intensity')
ax_cdf.set_yticks(np.linspace(0, 1, 5))

# prevent overlap of y-axis labels
fig.tight_layout()
plt.show()

# if __name__ == "__main__":
    # list3Task2()
    # plot_img_and_hist()