import math
import numpy as np
import matplotlib.pyplot as plt
import skimage
from skimage import io
from skimage import data
from skimage import transform

def przesuniecie():
    # Image Translation - przesunięcie obrazu
    img = io.imread('Einstein.jpg')
    rows, cols = img.shape
    H = transform.SimilarityTransform(scale=1, rotation=0,
                                              translation=(-100, -50))
    H1 = transform.EuclideanTransform(
       rotation = 0,
       translation = (-100, -50)
       )
    img_trans = transform.warp(img, H)

    fig, ax = plt.subplots(ncols=2)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(img_trans, cmap=plt.cm.gray)
    plt.show()

def odbicie():
    # Image Reflection - odbicie opbrazu

    img = io.imread('Einstein.jpg')
    rows, cols = img.shape
    # flip image horizontally
    H1 = np.array([[1, 0, 0], [0, -1, rows], [0, 0, 1]])
    reflected_img1 = transform.warp(img, H1)
    # flip image vertically
    H2 = np.array([[-1, 0, cols], [0, 1, 0], [0, 0, 1]])
    reflected_img2 = transform.warp(img, H2)

    # inaczej odbicie horyzontalne
    src = np.array([[0, 0], [0, rows], [cols, rows], [cols, 0]])
    dst = np.array([[0, rows], [0, 0], [cols, 0], [cols, rows]])

    H3 = transform.AffineTransform()
    H3.estimate(src, dst)
    reflected_img3 = transform.warp(img, H3)

    fig, ax = plt.subplots(ncols=3)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(reflected_img1, cmap=plt.cm.gray)
    ax[2].imshow(reflected_img2, cmap=plt.cm.gray)
    plt.show()

def obrot():
    # Image Rotation - obrót obrazu

    img = io.imread('Einstein.jpg')
    rows, cols = img.shape
    theta = np.pi / 4
    # definiowanie macierzy transformacji
    H1 = np.array([[np.cos(theta), -np.sin(theta), rows / 2],
                   [np.sin(theta), np.cos(theta), cols / 2],
                   [0, 0, 1]])
    H2 = np.array([[1, 0, -rows / 2],
                   [0, 1, -cols / 2],
                   [0, 0, 1]])
    # lub za pomocą funkcji
    H1 = transform.EuclideanTransform(
        rotation=theta,
        translation=(rows / 2, cols / 2)
    )
    H2 = transform.EuclideanTransform(
        rotation=0,
        translation=(-rows / 2, -cols / 2)
    )
    H = np.dot(H1, H2)

    img_rotation = transform.warp(img, H)

    fig, ax = plt.subplots(ncols=2)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(img_rotation, cmap=plt.cm.gray)
    plt.show()

def skalowanie():
    # Image Scaling - skalowanie obrazu

    img = io.imread('Einstein.jpg')
    rows, cols = img.shape

    image_rescaled = transform.rescale(img, 0.5)
    image_resized = transform.resize(img, (200, 250))

    fig, ax = plt.subplots(ncols=3)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(image_rescaled, cmap=plt.cm.gray)
    ax[2].imshow(image_resized, cmap=plt.cm.gray)
    plt.show()

def idkCoTo():
    text = data.text()

    tform = transform.SimilarityTransform(scale=1, rotation=math.pi / 4,
                                          translation=(text.shape[0] / 2, -100))

    rotated = transform.warp(text, tform)
    back_rotated = transform.warp(rotated, tform.inverse)

    fig, ax = plt.subplots(nrows=3)

    ax[0].imshow(text, cmap=plt.cm.gray)
    ax[1].imshow(rotated, cmap=plt.cm.gray)
    ax[2].imshow(back_rotated, cmap=plt.cm.gray)

    for a in ax:
        a.axis('off')

    plt.tight_layout()
    plt.show()

def idkCoTo1():
    text = data.text()

    src = np.array([[0, 0], [0, 50], [200, 50], [200, 0]])
    dst = np.array([[155, 15], [65, 40], [260, 130], [360, 95]])

    tform3 = transform.ProjectiveTransform()
    tform3.estimate(src, dst)
    warped = transform.warp(text, tform3, output_shape=(50, 200))

    fig, ax = plt.subplots(nrows=2, figsize=(8, 3))

    ax[0].imshow(text, cmap=plt.cm.gray)
    ax[0].plot(dst[:, 0], dst[:, 1], '.r')
    ax[1].imshow(warped, cmap=plt.cm.gray)

    for a in ax:
        a.axis('off')

    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    idkCoTo1()