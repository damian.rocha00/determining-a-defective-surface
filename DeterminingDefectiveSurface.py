import numpy as np
import matplotlib.pyplot as plt
from skimage import morphology, measure, io, color, filters, draw

index = 0
photo = io.imread('materials/kwadrat5x5Fill.jpg')  # Wczytaj photo
pixel_size_cm = (0.1 / 28)  # Przykładowy rozmiar piksela w centymetrach
if photo.shape[-1] == 4:  # Jeśli liczba kanałów wynosi 4, to jest to RGBA
    photo = color.rgba2rgb(photo)   # Convert RGBA to RGB
gray_color_photo = color.rgb2gray(photo)

fig, (ax1, ax2) = plt.subplots(1, 2)

def originalView():
    ax1.imshow(photo)
    ax1.set_title('Oryginalne zdjęcie')

def grayColorView():
    ax2.imshow(gray_color_photo, cmap=plt.cm.gray)
    ax2.set_title('photo w skali szarości')

def histView():
    fig, ax_hist = plt.subplots()  # Tworzenie nowego subplotu tylko dla histogramu
    ax_hist.hist(gray_color_photo.ravel(), bins=256)
    ax_hist.set_title('Histogram obrazu')
    plt.show()

def setThresholds():
    threshold1 = 0.39
    threshold2 = 0.50
    return threshold1, threshold2

def binaryPhotoThreshold1View():
    threshold1, _ = setThresholds()
    binary_photo1 = gray_color_photo < threshold1
    ax1.imshow(binary_photo1, cmap=plt.cm.gray)
    ax1.set_title(f'Obraz binarny 1, Threshold {threshold1}')
    return binary_photo1

def binaryPhotoThreshold2View():
    _, threshold2 = setThresholds()
    binary_obraz2 = gray_color_photo > threshold2
    ax2.imshow(binary_obraz2, cmap=plt.cm.gray)
    ax2.set_title(f'photo binarny 2, Threshold {threshold2}')

def binaryOtsuView():
    threshold = filters.threshold_otsu(gray_color_photo)
    binary = gray_color_photo > threshold
    ax1.imshow(binary, cmap=plt.cm.gray)
    ax1.set_title('photo binarny Otsu')

def morphologyView():
    binary_photo1 = binaryPhotoThreshold1View()
    rounded_edges = morphology.dilation(binary_photo1, morphology.disk(10))
    ax1.imshow(rounded_edges, cmap=plt.cm.gray)
    ax1.set_title('photo morfologiczny')
    return rounded_edges

def contourMorphologyView():
    rounded_edges = morphologyView()
    ax2.imshow(rounded_edges, cmap=plt.cm.gray)
    ax2.set_title('photo morfologiczny z obrysem')
    contours = measure.find_contours(rounded_edges, 0.1)
    for contour in contours:
        ax2.plot(contour[:, 1], contour[:, 0], linewidth=1, color='red')
    return contours

def calculate_contour_dimensions(contour):
    min_y, min_x = np.min(contour, axis=0)
    max_y, max_x = np.max(contour, axis=0)
    height = max_y - min_y
    width = max_x - min_x
    return height, width

def pixelsToCentimeters(contour):
    height_px, width_px = calculate_contour_dimensions(contour)
    height_cm = height_px * pixel_size_cm
    width_cm = width_px * pixel_size_cm
    return height_cm, width_cm

def on_key(event):
    global index
    if event.key == 'right':
        index = (index + 1) % 5  # Przełącz między dwoma widokami
    elif event.key == 'left':
        index = (index - 1) % 5  # Przełącz między dwoma widokami

    ax1.clear()
    ax2.clear()
    if index == 0:
        originalView()
        grayColorView()
    elif index == 1:
        histView()
    elif index == 2:
        binaryPhotoThreshold1View()
        binaryPhotoThreshold2View()
    elif index == 3:
        binaryOtsuView()
    elif index == 4:
        morphologyView()
        contours = contourMorphologyView()
        if contours:  # Sprawdź, czy kontury istnieją, zanim spróbujesz je wydrukować
            for i, contour in enumerate(contours):
                height_px, width_px = calculate_contour_dimensions(contour)
                height_cm, width_cm = pixelsToCentimeters(contour)
                print(f"Kontur {i + 1}:")
                print("  Długość (height): {:.2f} pikseli, {:.2f} cm".format(height_px, height_cm))
                print("  Szerokość (width): {:.2f} pikseli, {:.2f} cm".format(width_px, width_cm))


    plt.draw()

def woodDecsk():
    setThresholds()

    originalView()
    grayColorView()

    # Dodaj obsługę zdarzeń klawiatury
    plt.gcf().canvas.mpl_connect('key_press_event', on_key)

    plt.show()

if __name__ == "__main__":
    woodDecsk()
