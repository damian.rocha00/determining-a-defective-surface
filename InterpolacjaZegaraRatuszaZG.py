import matplotlib.pyplot as plt
from skimage import io
import numpy as np
from scipy.ndimage import zoom
def printRaturHour():
    ratuszZG = io.imread('ratuszZG.jpg')
    zegar = ratuszZG[180:220, 380:420, :]

    # fig, ax = plt.subplots(1, 1, figsize=(10, 5), sharex=True, sharey=True)
    # im1 = ax.imshow(zegar)

    # Powiększanie obrazu przy użyciu bilinearnej interpolacji dla każdego kanału koloru niezależnie
    enlarged_image = np.zeros((zegar.shape[0]*2, zegar.shape[1]*2, zegar.shape[2]), dtype=np.uint8)
    for i in range(zegar.shape[2]):
        enlarged_image[:,:,i] = zoom(zegar[:,:,i], (2, 2), order=1)

    fig, ax = plt.subplots(1, 2, figsize=(12, 6))
    ax[0].imshow(zegar)
    ax[0].set_title('Oryginalny obraz')

    ax[1].imshow(enlarged_image)
    ax[1].set_title('Powiększony obraz')

    plt.show()



if __name__ == "__main__":
    printRaturHour()