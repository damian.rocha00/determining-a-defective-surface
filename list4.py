import numpy as np
import matplotlib.pyplot as plt
import math


def basic():
    # wierzchołki figury
    x = np.array([0, 2, 4, 2, 0])
    y = np.array([2, 0, 2, 4, 2])
    A = np.array([x, y, np.ones(5)])
    # definicja transforamcji
    phi = math.pi / 4
    xs = (np.min(x) + np.max(x)) / 2
    ys = (np.min(y) + np.max(y)) / 2
    H1 = np.array([[1, 0, -xs], [0, 1, -ys], [0, 0, 1]])
    H2 = np.array([[np.cos(phi), -np.sin(phi), 0], [np.sin(phi), np.cos(phi), 0], [0, 0, 1]])
    H3 = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])
    H = np.dot(H3, np.dot(H2, H1))
    # transformacja figury
    Anew = np.dot(H, A)
    xnew = Anew[0, :]
    ynew = Anew[1, :]
    print(xnew[:-1])
    print(ynew[:-1])
    # plot
    plt.plot(x, y, 'b', xnew, ynew, 'r')
    plt.grid(True)
    plt.axis('square')
    plt.show()


def odlMiedzyPunktami(x1, y1, x2, y2):
    import math
    roz_y = y1 - y2
    roz_x = x1 - x2
    return math.sqrt(roz_x * roz_x + roz_y * roz_y)
def task3():
    # wierzchołki figury
    x = np.array([0, 2, 4, 2, 0])
    y = np.array([2, 0, 2, 4, 2])
    A = np.array([x, y, np.ones(5)])
    # definicja transforamcji
    phi = -(math.pi / 4)
    sigma = odlMiedzyPunktami(0,2,2,0)/2
    xs = (2)
    ys = (0)
    przesuniecie = np.array([[1, 0, -xs], [0, 1, -ys], [0, 0, 1]])
    obrot = np.array([[np.cos(phi), -np.sin(phi), 0], [np.sin(phi), np.cos(phi), 0], [0, 0, 1]])
    cofnieciePrzesuniecia = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])
    skalowanie = np.array([[sigma, 0, 0], [0, sigma, 0], [0, 0, sigma]])
    przesuniecie2 = np.array([[1, 0, -0.6], [0, 1, 0], [0, 0, 1]])
    H = np.dot(przesuniecie2 , np.dot(cofnieciePrzesuniecia ,np.dot(skalowanie, np.dot(obrot, przesuniecie))))
    # transformacja figury
    Anew = np.dot(H, A)
    xnew = Anew[0, :]
    ynew = Anew[1, :]
    print(xnew[:-1])
    print(ynew[:-1])
    # plot
    plt.plot(x, y, 'b', xnew, ynew, 'r')
    plt.grid(True)
    plt.axis('square')
    plt.show()

def task4():
    # wierzchołki figury
    x = np.array([0, 2, 4, 2, 0])
    y = np.array([2, 0, 2, 4, 2])
    A = np.array([x, y, np.ones(5)])
    # definicja transforamcji
    phi = -(math.pi / 3)
    sigma = odlMiedzyPunktami(0,2,2,0)/2
    xs = (2)
    ys = (4)
    przesuniecie = np.array([[1, 0, -xs], [0, 1, -ys], [0, 0, 1]])
    obrot = np.array([[np.cos(phi), -np.sin(phi), 0], [np.sin(phi), np.cos(phi), 0], [0, 0, 1]])
    cofnieciePrzesuniecia = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])
    skalowanie = np.array([[sigma, 0, 0], [0, sigma, 0], [0, 0, sigma]])
    # przesuniecie2 = np.array([[1, 0, -0.6], [0, 1, 0], [0, 0, 1]])
    H = np.dot(  cofnieciePrzesuniecia , np.dot(obrot, przesuniecie))
    # transformacja figury
    Anew = np.dot(H, A)
    xnew = Anew[0, :]
    ynew = Anew[1, :]
    print("Nowy x: ", xnew[:-1])
    print("Nowy y: ", ynew[:-1])
    print("Nowe A: ", Anew[0, 0],", ",Anew[1, 0])
    print("Nowe B: ", Anew[0, 1], ", ", Anew[1, 1])
    print("Nowe C: ", Anew[0, 2], ", ", Anew[1, 2])
    print("Nowe D: ", Anew[0, 3], ", ", Anew[1, 3])
    # plot
    plt.plot(x, y, 'b', xnew, ynew, 'r')
    plt.grid(True)
    plt.axis('square')
    plt.show()

def task5():
    # wierzchołki figury
    x = np.array([2, 4, 8, 8, 2])
    y = np.array([2, 4, 4, 0, 2])
    A = np.array([x, y, np.ones(5)])
    # definicja transforamcji
    phi = math.pi / 4
    xs = (2)
    ys = (2)
    sigma = 1/2
    H1 = np.array([[1, 0, -xs], [0, 1, -ys], [0, 0, 1]])
    H3 = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])  # zmień 1 na 3 jeśli chcesz aby się pokrywały
    skalowanie = np.array([[sigma, 0, 0], [0, sigma, 0], [0, 0, sigma]])
    H = np.dot(H3 ,np.dot(skalowanie, H1))
    # transformacja figury
    Anew = np.dot(H, A)
    xnew = Anew[0, :]
    ynew = Anew[1, :]
    print(xnew[:-1])
    print(ynew[:-1])
    print("Nowe A: x:", Anew[0, 0], ", y:", Anew[1, 0])
    print("Nowe B: x:", Anew[0, 1], ", y:", Anew[1, 1])
    print("Nowe C: x:", Anew[0, 2], ", y:", Anew[1, 2])
    print("Nowe D: x:", Anew[0, 3], ", y:", Anew[1, 3])
    # plot
    plt.plot(x, y, 'b', xnew, ynew, 'r')
    plt.grid(True)
    plt.axis('square')
    plt.show()

def task6A():
    # wierzchołki figury
    x = np.array([0, 0, 3, 5, 0])
    y = np.array([0, 3, 6, 5, 0])
    A = np.array([x, y, np.ones(5)])
    # definicja transforamcji
    phi = -(math.pi / 2)
    xp = np.array([5,5])
    yp = np.array([3,6])
    xs = (np.min(xp) + np.max(xp)) / 2
    ys = (np.min(yp) + np.max(yp)) / 2
    sigma = 1/2
    przesuniecie = np.array([[1, 0, -xs], [0, 1, -ys], [0, 0, 1]])
    obrot = np.array([[np.cos(phi), -np.sin(phi), 0], [np.sin(phi), np.cos(phi), 0], [0, 0, 1]])
    cofnieciePrzesuniecia = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])
    H3 = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])  # zmień 1 na 3 jeśli chcesz aby się pokrywały
    skalowanie = np.array([[sigma, 0, 0], [0, sigma, 0], [0, 0, 1]])
    H = np.dot(cofnieciePrzesuniecia, np.dot(obrot, przesuniecie))
    # transformacja figury
    Anew = np.dot(H, A)
    xnew = Anew[0, :]
    ynew = Anew[1, :]
    print(xnew[:-1])
    print(ynew[:-1])
    print("Nowe A: x:", Anew[0, 0], ", y:", Anew[1, 0])
    print("Nowe B: x:", Anew[0, 1], ", y:", Anew[1, 1])
    print("Nowe C: x:", Anew[0, 2], ", y:", Anew[1, 2])
    print("Nowe D: x:", Anew[0, 3], ", y:", Anew[1, 3])

    # plot
    plt.plot(x, y, 'b', xnew, ynew, 'r')
    plt.grid(True)
    plt.axis('square')
    plt.show()

def task6B():
        # wierzchołki figury
        x = np.array([0, 0, 3, 5, 3, 0])
        y = np.array([0, 3, 6, 5, 3, 0])
        A = np.array([x, y, np.ones(6)])
        # definicja transforamcji
        phi = 0 # było -(math.pi / 2)
        xp = np.array([5, 5])
        yp = np.array([3, 6])
        xs = (np.min(xp) + np.max(xp)) / 2
        ys = (np.min(yp) + np.max(yp)) / 2
        xsigma = 1 / 2
        ysigma = 2
        przesuniecie = np.array([[1, 0, -xs], [0, 1, -ys], [0, 0, 1]])
        obrot = np.array([[np.cos(phi), -np.sin(phi), 0], [np.sin(phi), np.cos(phi), 0], [0, 0, 1]])
        cofnieciePrzesuniecia = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])
        H3 = np.array([[1, 0, xs], [0, 1, ys], [0, 0, 1]])  # zmień 1 na 3 jeśli chcesz aby się pokrywały
        skalowanie = np.array([[xsigma, 0, 0], [0, ysigma, 0], [0, 0, 1]])
        H = np.dot(obrot, np.dot(skalowanie, np.dot(cofnieciePrzesuniecia, przesuniecie)))
        # transformacja figury
        Anew = np.dot(H, A)
        xnew = Anew[0, :]
        ynew = Anew[1, :]
        print(xnew[:-1])
        print(ynew[:-1])
        print("Nowe A: x:", Anew[0, 0], ", y:", Anew[1, 0])
        print("Nowe B: x:", Anew[0, 1], ", y:", Anew[1, 1])
        print("Nowe C: x:", Anew[0, 2], ", y:", Anew[1, 2])
        print("Nowe D: x:", Anew[0, 3], ", y:", Anew[1, 3])
        print("Nowe E: x:", Anew[0, 4], ", y:", Anew[1, 4])

        # plot
        plt.plot(x, y, 'b', xnew, ynew, 'r')
        plt.grid(True)
        plt.axis('square')
        plt.show()

if __name__ == '__main__':
    task6B()