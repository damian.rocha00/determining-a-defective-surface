import matplotlib.pyplot as plt
from skimage import io
import numpy as np


def task1_printDworekIn3Colors():
    dworek = io.imread('dworek.jpg')
    (m, n, p) = dworek.shape
    red = np.zeros((m, n, p), dtype='uint8')
    green = np.zeros((m, n, p), dtype='uint8')
    blue = np.zeros((m, n, p), dtype='uint8')

    red[:, :, 0] = dworek[:, :, 0]
    green[:, :, 1] = dworek[:, :, 1]
    blue[:, :, 2] = dworek[:, :, 2]

    fig, ax = plt.subplots(2, 2, figsize=(10, 5), sharex=True, sharey=True)
    im1 = ax[0, 0].imshow(dworek)
    im2 = ax[0, 1].imshow(red)
    im3 = ax[1, 0].imshow(green)
    im4 = ax[1, 1].imshow(blue)
    plt.show()

def task2_printDom():
    dom = io.imread('dom.jpg')
    domBezOknaPiwnicznego = io.imread('dom.jpg')
    domBezOknaPiwnicznego[535:575, 75:150, :] = dom[495:535, 75:150, :]
    fig, ax = plt.subplots(1, 2, figsize=(10, 5), sharex=True, sharey=True)
    im1 = ax[0].imshow(dom)
    im2 = ax[1].imshow(domBezOknaPiwnicznego)
    plt.show()

def task3_print():
    dworekJpg = io.imread('dworek.jpg')
    R = dworekJpg[:, :, 0]
    G = dworekJpg[:, :, 1]
    B = dworekJpg[:, :, 2]
    sredniaArytmetyczna = "Średnia arytmetyczna= ", ( R + G + B) / 3
    sredniaWazonaSkladowych = "Średnia ważona składowych= ", 0.299*R + 0.587*G + 0.114*B
    print(sredniaArytmetyczna)
    print(sredniaWazonaSkladowych)
    # plt.imshow(dworek)
    # plt.show()

if __name__ == "__main__":
    # task1_printDworekIn3Colors()
    # task2_printDom()
    task3_print()