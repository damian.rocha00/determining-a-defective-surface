import numpy as np
import matplotlib.pyplot as plt
from skimage import color, io

# Wczytaj photo
photo = io.imread('materials/kwadrat5x5.jpg')
gray_color_photo = color.rgb2gray(photo)

# Inicjalizacja indeksu do nawigacji między widokami
index = 0

# Funkcja do wyświetlania oryginalnego zdjęcia
def originalView():
    ax1.imshow(photo, cmap='gray')  # Wyświetl oryginalne zdjęcie w skali szarości
    ax1.set_title('Oryginalne zdjęcie')

# Funkcja do wyświetlania zdjęcia w skali szarości
def grayColorView():
    ax2.imshow(gray_color_photo, cmap='gray')  # Wyświetl zdjęcie w skali szarości
    ax2.set_title('Zdjęcie w skali szarości')

# Funkcja do obsługi zdarzeń klawiatury
def on_key(event):
    global index
    if event.key == 'right':
        index = (index + 1) % 3  # Przełącz między trzema widokami
    elif event.key == 'left':
        index = (index - 1) % 3  # Przełącz między trzema widokami

    if index == 0:
        originalView()  # Wyświetl oryginalne zdjęcie
        grayColorView()
        ax2.clear()  # Wyczyść drugi subplot
    elif index == 1:
        grayColorView()  # Wyświetl zdjęcie w skali szarości
        ax1.clear()  # Wyczyść pierwszy subplot
    plt.draw()

# Utwórz subploty
fig, (ax1, ax2) = plt.subplots(1, 2)

# Wyświetl oryginalne zdjęcie na początku
originalView()

# Dodaj obsługę zdarzeń klawiatury
plt.gcf().canvas.mpl_connect('key_press_event', on_key)

plt.show()
