import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

def przesuniecie():
    # Image Translation - przesunięcie obrazu
    img = cv.imread('Einstein.jpg', 0)
    rows, cols = img.shape
    H = np.float64([
        [1, 0, 100],
        [0, 1, 50 ]
    ])
    translation = cv.warpAffine(img, H, (cols, rows))
    fig, ax = plt.subplots(ncols=2)
    cv.imwrite('translation.jpg', translation)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(translation, cmap=plt.cm.gray)
    plt.show()

def odbicie():
    # Image Reflection - odbicie opbrazu

    img = cv.imread('Einstein.jpg', 0)
    rows, cols = img.shape
    # flip image horizontally
    H1 = np.float32([[1, 0, 0], [0, -1, rows], [0, 0, 1]])
    reflected_img1 = cv.warpPerspective(img, H1, (cols, rows))
    # flip image vertically
    H2 = np.float32([[-1, 0, cols], [0, 1, 0], [0, 0, 1]])
    reflected_img2 = cv.warpPerspective(img, H2, (cols, rows))

    fig, ax = plt.subplots(ncols=3)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(reflected_img1, cmap=plt.cm.gray)
    ax[2].imshow(reflected_img2, cmap=plt.cm.gray)
    plt.show()

def obrot():
    # Image Rotation - obrót obrazu

    img = cv.imread('Einstein.jpg', 0)
    rows, cols = img.shape

    H = np.float32([[1, 0, 0], [0, -1, rows], [0, 0, 1]])
    img_rotation = cv.warpAffine(img, cv.getRotationMatrix2D((cols / 2, rows / 2), 45, 1), (cols, rows))

    H2 = np.concatenate([cv.getRotationMatrix2D((cols / 2, rows / 2), 45, 1), np.array([[0, 0, 1]])], axis=0)
    img_rotation2 = cv.warpPerspective(img, H2, (cols, rows))

    fig, ax = plt.subplots(ncols=2)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(img_rotation, cmap=plt.cm.gray)
    plt.show()

def skalowanie():
    # Image Scaling - skalowanie obrazu

    img = cv.imread('Einstein.jpg', 0)
    rows, cols = img.shape

    img_shrinked = cv.resize(img, (250, 200), interpolation=cv.INTER_AREA)
    img_enlarged = cv.resize(img, None, fx=0.5, fy=0.5, interpolation=cv.INTER_CUBIC)

    fig, ax = plt.subplots(ncols=3)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(img_shrinked, cmap=plt.cm.gray)
    ax[2].imshow(img_enlarged, cmap=plt.cm.gray)
    plt.show()

def pochylenie():
    # Image Shearing in X-Axis - pochylenie obrazu wzdłuż osi X

    img = cv.imread('Einstein.jpg', 0)
    rows, cols = img.shape

    Mx = np.float32([[1, 0.5, 0], [0, 1, 0], [0, 0, 1]])
    sheared_x_img = cv.warpPerspective(img, Mx, (int(1.5 * cols), int(1 * rows)))

    # Image Shearing in Y-Axis - pochylenie obrazu wzdłuż osi Y

    My = np.float32([[1, 0, 0], [0.5, 1, 0], [0, 0, 1]])
    sheared_y_img = cv.warpPerspective(img, My, (int(1 * cols), int(1.6 * rows)))

    fig, ax = plt.subplots(ncols=3)

    ax[0].imshow(img, cmap=plt.cm.gray)
    ax[1].imshow(sheared_x_img, cmap=plt.cm.gray)
    ax[2].imshow(sheared_y_img, cmap=plt.cm.gray)
    plt.show()


if __name__ == "__main__":
    przesuniecie()