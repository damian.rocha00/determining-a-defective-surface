import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage import color
from scipy.ndimage import zoom


def woodDecsk():
    obraz = io.imread('woodDesck.PNG')  # Load image directly as grayscale
    rgb_image = color.rgba2rgb(obraz)  # Convert RGBA to RGB
    gray_color_obraz = color.rgb2gray(rgb_image)

    # Definiowanie współrzędnych punktów docelowych
    x_range = np.linspace(0, len(gray_color_obraz[0]) - 1, len(gray_color_obraz[0]) * 2)
    y_range = np.linspace(0, len(gray_color_obraz) - 1, len(gray_color_obraz) * 2)

    # Powiększanie obrazu przy użyciu bilinearnej interpolacji
    enlarged_image = zoom(gray_color_obraz, (2, 2), order=1)

    fig, ax = plt.subplots(1, 2, figsize=(12, 6))
    ax[0].imshow(gray_color_obraz, cmap='gray')
    ax[0].set_title('Oryginalny obraz')

    ax[1].imshow(enlarged_image, cmap='gray')
    ax[1].set_title('Powiększony obraz')

    plt.show()

if __name__ == "__main__":
    woodDecsk()

